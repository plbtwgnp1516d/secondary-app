package com.plbtw.lacakindonesia;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 高橋六羽 on 6/2/2016.
 */
public class CariAdapter extends ArrayAdapter<KontenCari> {
    Context context;
    int resLayout;
    List<KontenCari> listCari;

    TextView tvJudulCari;
    TextView tvLokasiCari;
    TextView tvTagsCari;
    KontenCari navLisCari;
    ImageView ivCari;

    public CariAdapter(Context context, int resLayout, List<KontenCari> listCari){
        super(context, resLayout, listCari);
        this.context=context;
        this.resLayout=resLayout;
        this.listCari=listCari;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {

        View v = View.inflate(context,resLayout,null);

        tvJudulCari = (TextView)v.findViewById(R.id.txtJudulCari);
        tvLokasiCari = (TextView)v.findViewById(R.id.txtLokasiCari);
        tvTagsCari = (TextView)v.findViewById(R.id.txtTagsCari);
        ivCari = (ImageView)v.findViewById(R.id.imgCari);

        navLisCari = listCari.get(position);

        tvJudulCari.setText(navLisCari.getJudul_Konten());
        tvLokasiCari.setText(navLisCari.getLokasi());
        tvTagsCari.setText("Tags : " + navLisCari.getTags());
        Glide.with(getContext()).load(navLisCari.getGambar_Wisata()).into(ivCari);

        return v;
    }
}
