package com.plbtw.lacakindonesia;

/**
 * Created by 高橋六羽 on 6/2/2016.
 */
public class KontenCari {
    String Nama_Wisata;
    String Detail_Wisata;
    String Judul_Konten;
    String Gambar_Wisata;
    int Jumlah_Votes;
    String Tags;
    String Lokasi;

    public String getNama_Wisata() {
        return Nama_Wisata;
    }

    public void setNama_Wisata(String nama_Wisata) {
        Nama_Wisata = nama_Wisata;
    }

    public String getDetail_Wisata() {
        return Detail_Wisata;
    }

    public void setDetail_Wisata(String detail_Wisata) {
        Detail_Wisata = detail_Wisata;
    }

    public String getJudul_Konten() {
        return Judul_Konten;
    }

    public void setJudul_Konten(String judul_Konten) {
        Judul_Konten = judul_Konten;
    }

    public String getGambar_Wisata() {
        return Gambar_Wisata;
    }

    public void setGambar_Wisata(String gambar_Wisata) {
        Gambar_Wisata = gambar_Wisata;
    }

    public int getJumlah_Votes() {
        return Jumlah_Votes;
    }

    public void setJumlah_Votes(int jumlah_Votes) {
        Jumlah_Votes = jumlah_Votes;
    }

    public String getTags() {
        return Tags;
    }

    public void setTags(String tags) {
        Tags = tags;
    }

    public String getLokasi() {
        return Lokasi;
    }

    public void setLokasi(String lokasi) {
        Lokasi = lokasi;
    }
}
