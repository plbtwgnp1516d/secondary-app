package com.plbtw.lacakindonesia;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.plbtw.lacakindonesia.Rest.RestClient;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    EditText txtCari;
    Button bCari;
    private Call<APIKontenCari> call;
    private RestClient.GitApiInterface service;

    private ListView lvCari;
    private CariAdapter cariAdapter;
    private List<KontenCari> cariItems = new ArrayList<KontenCari>();

    private String MYAPIKEY = "ExYctdZGA7HQWaXozurM14";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtCari = (EditText)findViewById(R.id.etCari);
        bCari = (Button)findViewById(R.id.btnCari);

        lvCari = (ListView) findViewById(R.id.listViewCari);

        cariAdapter = new CariAdapter(getApplicationContext(), R.layout.item_cari, cariItems);

        lvCari.setAdapter(cariAdapter);

        bCari.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v==bCari)
        {
            fetchData(txtCari.getText().toString());
        }
    }

    public void fetchData(String key)
    {
        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Mencari Tags...");
        progressDialog.show();

        service = RestClient.getClient();
        call = service.getKontenbyTags(key, MYAPIKEY);
        call.enqueue(new Callback<APIKontenCari>() {
            @Override
            public void onResponse(Response<APIKontenCari> response) {
                Log.d("NewFragment", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    // request successful (status code 200, 201)
                    APIKontenCari result = response.body();
                    Log.d("NewFragment", "response = " + new Gson().toJson(result));
                    if (result != null) {

                        cariItems.clear();
                        List<KontenCari> cariResponseItems = result.getKontenData();

                        for (KontenCari cariResponseItem : cariResponseItems) {
                            cariItems.add(cariResponseItem);
                            cariAdapter.notifyDataSetChanged();
                        }

                        progressDialog.dismiss();
                    }

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    Toast.makeText(getApplicationContext(), "Koneksi Ke Internet Gagal", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getApplicationContext(), "Koneksi Ke Internet Gagal", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });

    }
}
