package com.plbtw.lacakindonesia.Rest;

/**
 * Created by Yoshua on 4/21/2016.
 */

import com.plbtw.lacakindonesia.APIKontenCari;
import com.plbtw.lacakindonesia.Helper.ToStringConverter;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import java.io.IOException;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.http.GET;
import retrofit.http.Query;

public class RestClient {

    private static GitApiInterface gitApiInterface ;
    private static String baseUrl = "http://hnwtvc.com" ;

    public static GitApiInterface getClient() {
        if (gitApiInterface == null) {

            OkHttpClient okClient = new OkHttpClient();
            okClient.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response response = chain.proceed(chain.request());
                    return response;
                }
            });

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverter(String.class, new ToStringConverter())
                    .client(okClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            gitApiInterface = client.create(GitApiInterface.class);
        }
        return gitApiInterface ;
    }

    public interface GitApiInterface {

        @GET("/tournesia-rest/index.php/public/wisata/kontenbyTags")
        Call<APIKontenCari> getKontenbyTags(@Query("tags") String tags, @Query("api_key") String api_key);

    }

}



