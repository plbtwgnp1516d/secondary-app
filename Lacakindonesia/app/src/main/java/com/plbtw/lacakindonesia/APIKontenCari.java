package com.plbtw.lacakindonesia;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 高橋六羽 on 5/24/2016.
 */
@SuppressWarnings("serial")
public class APIKontenCari extends APIBaseResponse implements Serializable{

    List<KontenCari> kontenData;

    public List<KontenCari> getKontenData() {
        return kontenData;
    }

    public void setKontenData(List<KontenCari> kontenData) {
        this.kontenData = kontenData;
    }
}
